FROM rust:1.57

RUN cargo install cargo-c
RUN apt-get update -y
RUN apt-get install -y mingw-w64
RUN rustup target add x86_64-pc-windows-gnu
RUN rustup toolchain install stable-x86_64-pc-windows-gnu
RUN rustup component add rustfmt clippy

